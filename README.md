#Riconoscitore Linguaggi regolari, Weakly o Strongly Irreversible #

Creazione di un programma che, tramite l'ausilio di libreria FAdo, permetta di riconoscere se un linguaggio riconosciuto da un automa deterministico sia debolmente o fortemente irreversible.


### Codice e gestione repository###

* irreversibleWeaklyStrongly.py -> contiene metodo di riconoscimento e sviluppo in merito a weakyly e strongly irreversible. Ultima versione: utilizzato product per sviluppare A^ con Ar x ARScc
* esempiFA: contiene esempi di automi deterministici da dare in input in lettura al programma sopracitato
* codiceOld: contiene porizioni di codice usate precedentemente per versioni datate del programma principale