#!/usr/bin/env python
# -*- coding: utf-8 -*-
from FAdo.fa import *
from FAdo.reex import *
from FAdo.fio import *

""" Obiettivo: creare automa e definire se esso rappresenta l'utilizzo
    di un linguaggio debolmente o fortemente irriversibile"""

def automaReverse(automa):
    """Restituisce l'automa inverso dato in input
    input = DFA
    return = NFA"""
    automaR = automa.toNFA()
    automaR = automaR.reversal()
    print '\nAutoma invertito:\n{}'.format(automaR)
    return automaR

def automaScc(automa):
    """Dato l'automa, calcola il corrispettivo automa SCC
    input = NFA
    return = NFA"""
    automaSCC = automa.dup()
    listaSCC = automaSCC.stronglyConnectedComponents()
    listaT = automaSCC.succintTransitions()
    #Controllo se le transizioni collegano stati negli stessi SCC, se cosi non fosse
    #vengono rimosse
    for transizione in listaT:
        sameSCC = False
        stateIN = automaSCC.stateIndex(transizione[0])
        sym = transizione[1]
        stateOUT = automaSCC.stateIndex(transizione[2])
        for scc in listaSCC:
            if stateIN in scc and stateOUT in scc:
                sameSCC = True
                break
        if sameSCC == False:
            automaSCC.delTransition(stateIN, sym, stateOUT)
    print '\nAutoma invertito SCC:\n{}'.format(automaSCC)
    return automaSCC

def automaACap(automa):
    """Creazione di Automa A^ come prodotto di AutomaR x AutomaRSCC
    input = DFA
    output = NFA , automaCap"""
    def _settaggioStatiAutomaCap(prodotto):
        """Metodo che permette di eliminare gli stati creati con empty, settando stati finali ed iniziali
        input = NFA
        return = NFA"""
        prodotto.setInitial([])
        #Cancello stati creati con empty, aggiungo stati iniziali e finali
        listaStatiCancellare = []
        listaSCC = automaSCC.stronglyConnectedComponents()
        for statoEsaminato in prodotto.States:
            indice = prodotto.stateIndex(statoEsaminato)
            if "@empty_set" in statoEsaminato: #cancello stati associati empty
                listaStatiCancellare.append(indice)
            else:
                prodotto.addFinal(indice)
                if statoEsaminato[0] == statoEsaminato[1]:
                    prodotto.addInitial(indice)
        prodotto.deleteStates(listaStatiCancellare)
        #Aggiungo stati p,p non creati da prodotto
        for stato in automa.States:
            tupla = (stato,stato)
            if tupla not in prodotto.States:
                prodotto.addState(tupla)
                prodotto.addInitial(prodotto.stateIndex(tupla))
                prodotto.addFinal(prodotto.stateIndex(tupla))
        return prodotto

    def _rimuovoTransizioni(prodotto):
        """Rimosse le transizioni superflue dall automa prodotto, in base a transizione se parte da p e finisce in p,
        oppure se da uno stato di partenza qualsiasi finisce in p,p
        input = NFA
        return = NFA"""
        listaTransizioni = prodotto.succintTransitions()
        for transizione in listaTransizioni: #transizione tupla con statoin, sym, statoout --> stati tipo stringa, stato index serve tupla
            tuplaIN = (transizione[0][2],transizione[0][7]) 
            indiceIN = prodotto.stateIndex(tuplaIN)
            sym = transizione[1]
            tuplaOUT = (transizione[2][2],transizione[2][7])
            indiceOUT = prodotto.stateIndex(tuplaOUT)
            transizioneStessoStato = tuplaIN == tuplaOUT and transizione[0][2] == transizione[0][7]
            statoOutPP = transizione[2][2] == transizione[2][7]
            if transizioneStessoStato or statoOutPP:
                prodotto.delTransition(indiceIN, sym, indiceOUT)
        return prodotto
    
    #Trovo automa inverso-----------------------------------------------------------
    automaR = automaReverse(automa)
    #Visualizza a grafico
    automaR.display()
    
    #Trovo automa inverso con delta SCC---------------------------------------------
    automaSCC = automaScc(automaR)
    #Visualizza a grafico
    automaSCC.display()

    #Creazione Automa A^ = Ar X ArScc
    prodotto = automaR.product(automaSCC)
    
    #Settaggio automa con stati iniziali e finali
    prodotto = _settaggioStatiAutomaCap(prodotto)
    
    #Rimuovo transizioni non utili in base alla funzione di transizione
    prodotto = _rimuovoTransizioni(prodotto)
    #Rinomino stati
    for stato in prodotto.States:
        indice = prodotto.stateIndex(stato)
        prodotto.renameState(indice, stato[0]+' - '+stato[1])
    print '\nAutoma A^:\n{}\n'.format(prodotto)
    return prodotto
    
def verificaCiclo(automa):
    """Dato in input un automa, verifica se è presente un ciclo dato da piu stati appartenenti allo stesso SCC, oppure dal cilo di uno stesso stato su se stesso
    input: FA
    return: True (presente ciclo) , False (altrimenti)"""
    listaSCC = automa.stronglyConnectedComponents()
    for scc in listaSCC:
        if len(scc) > 1 :
            return True
    listaT = automa.succintTransitions()
    for transizione in listaT:
        stateIN = automa.stateIndex(transizione[0])
        stateOUT = automa.stateIndex(transizione[2])
        if stateIN == stateOUT:
            return True
    return False

def weaklyOrStronglyIrreversible():
    """Funzione che, creato e analizzato un automa, riconosce se il linguaggio dell'automa stesso 
    è debolmente o fortemente irreversibile"""
    s = raw_input ('Nome del file in cui è salvato l automa: ')
    automa = readFromFile(s)[0]
    print '\nAutoma:\n{}'.format(automa)
   
    #Visualizza a grafico
    automa.display()

    #Verifica se automa è reversibile
    if automa.possibleToReverse() == True:
        print 'Automa reversbile! Terminazione programma...'
        quit()

    #Trova automa minimo e stampalo-------------------------------------------------
    com = automa.minimalNCompleteP()
    if com == False:
        minimoA = automa.minimal()
        print minimoA
    
    #Creazione Automa^ = Ar x Arscc------------------------------------------------
    automaCap = automaACap(automa)
    #Visualizza a grafico
    automaCap.display()
    
    #Valutazione linguaggio automa A^ ----------------------------------------
    ciclo = verificaCiclo(automaCap)
    if ciclo:
        print 'Automa è strongly-irreversible'
    else:
        lstringaMAX = ((len(automaCap)**2)-len(automaCap)/2)+1
        sAutoma = len(automaCap.enumNFA(lstringaMAX)[-1])
        print 'Automa è {}-irreversible'.format(sAutoma + 1)
if __name__ == '__main__':
    weaklyOrStronglyIrreversible()
