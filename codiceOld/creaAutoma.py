def creaAutoma(default = False):
    """ Metodo per creare e restituire un automa, in cui se il valore di default è Falso viene restituito un automa prestabilito a scelta,
    altrimenti verrà  creato
    input = str, si = crea, altrimenti scegli default
    return = DFA"""
    #Creo Automa Deterministico
    automa = DFA()
    if default == False:
        s = raw_input('Scegli automa:\n1)k-reversible\n2)Strongly-irreversible\n: ')
        if s == '1':
            #Aggiunta stati
            automa.addState('Q')
            automa.addState('P')
            automa.setInitial(0)
            automa.setFinal([1])
        
            #Aggiunta alfabeto
            automa.addSigma('a')
            automa.addSigma('b')
        
            #Aggiunta funzione delta
            automa.addTransition(0, 'a', 0)
            automa.addTransition(0, 'b', 1)
            automa.addTransition(1, 'b', 1)
        
            print 'Automa creato'
            return automa
        else:
            #Aggiunta stati
            automa.addState('Q')
            automa.addState('P')
            automa.setInitial(0)
            automa.setFinal([1])
        
            #Aggiunta alfabeto
            automa.addSigma('a')
            automa.addSigma('b')
            automa.addSigma('c')
        
            #Aggiunta funzione delta
            automa.addTransition(0, 'a', 0)
            automa.addTransition(0, 'b', 1)
            automa.addTransition(1, 'b', 1)
            automa.addTransition(1, 'a', 1)
            automa.addTransition(1, 'c', 0)
            
        
            print 'Automa creato'
            return automa
    else:
        #Aggiunta stati
        print '---Inserimento stati---'
        indiceStato = 0
        statoIniziale = False
        while True:
            s = raw_input('Nome dello stato: ')
            automa.addState(s)
            #Richiesta stato iniziale
            if statoIniziale == False:
                s = raw_input ('Stato iniziale? (si o no) ')
                s.lower()
                if s == 'si':
                    automa.setInitial(indiceStato)
                    statoIniziale = True
            #Richiesta stato finale
            s = raw_input('Stato finale? (si o no) ')
            s.lower()
            if s == 'si':
                automa.addFinal(indiceStato)
            indiceStato += 1
            #Chiedere inserire altro stato
            s = raw_input('Inserire altro stato? (si o no) ')
            s.lower()
            if s != 'si':
                break
            
        #Aggiunta alfabeto
        print '---Inserimento alfabeto---'
        while True:
            s = raw_input('Inserisci simbolo: ')
            automa.addSigma(s)
            s = raw_input('Inserire altro simbolo? (si o no) ')
            s.lower()
            if s != 'si':
                break
            
        #Aggiungi transizioni
        print '---Inserimento transizioni---'
        while True:
            s1 = raw_input('Aggiungi stato partenza: ')
            s2 = raw_input('Aggiungi simbolo transizione: ')
            s3 = raw_input('Aggiungi stato arrivo: ')
            automa.addTransition(automa.stateIndex(s1),s2,automa.stateIndex(s3))
            s = raw_input('Inserire altra transizione? (si o no) ')
            s.lower()
            if s != 'si':
                break
        return automa