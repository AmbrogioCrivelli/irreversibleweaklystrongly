def creaTransizioniAutomaACap(automaCap,indiceStato, automa, automaR, automaSCC):
    """Calcola l'automa prodotto A^ = Ar x ArSCC
    input = NFA, int (indice da cui partire l'analisi p,p), DFA, NFA, NFA
    return = NFA"""
    def _addTransizioniACap(indiceR, indiceSCC, indiceStato):
        """Aggiunge le transizioni all'automa A^ in base alla funzione di transizione
	input = int, int (indici per automa R e SCC), int (indice che indica stato da cui continuare l'analisi)
	return = NFA"""
        for simbolo in automaCap.Sigma:
            statiRaggiuntiR = automaR.evalSymbol([automaR.stateIndex(indiceR)],simbolo)
            if statiRaggiuntiR != set([]):
                statiRaggiuntiSCC = automaSCC.evalSymbol([automaSCC.stateIndex(indiceSCC)],simbolo)
                for sR in statiRaggiuntiR:
                    for sSCC in statiRaggiuntiSCC:
                        transizione = (indiceStato, simbolo, automaCap.stateIndex(automaR.States[sR]+' - '+automaSCC.States[sSCC]))
                        if sR != sSCC:
                            if (automaCap.States[indiceStato],simbolo,automaCap.States[automaCap.stateIndex(automaR.States[sR]+' - '+automaSCC.States[sSCC])]) in automaCap.succintTransitions():
                                break
                            automaCap.addTransition(indiceStato, simbolo, automaCap.stateIndex(automaR.States[sR]+' - '+automaSCC.States[sSCC]))
                            if automaCap.stateIndex(automaR.States[sR]+' - '+automaSCC.States[sSCC]) != indiceStato:
                                _addTransizioniACap(automaR.States[sR], automaSCC.States[sSCC], automaCap.stateIndex(automaR.States[sR]+' - '+automaSCC.States[sSCC]))
                            else:
                                break  
    #Creazione transizioni delta((r',r"),a) = {(p,q) appartengono a
    #deltaR(r',a) x deltaRScc(r",a) in cui pq diverso da p}  
        #3.1 - Trovo stato r,r da cui partire analisi
    statoAnalizzare = indiceStato
    for statoACap in automaCap.States:
        if statoACap == indiceStato + ' - ' + indiceStato:
            indiceStato = automaCap.stateIndex(statoACap) #indiceStato contiene indice dello stato da analizzare
            break
         #3.2 - Creo transizioni dell automa A^
    _addTransizioniACap(statoAnalizzare,statoAnalizzare, indiceStato)
    return automaCap

def automaACap (statiAutoma, automa, automaR, automaSCC):
    """Creazione dell'automa A^, considerando la possibilità di analizzare tutti gli stati p,p
    input = lst (lista stati automa), DFA, NFA, NFA
    return = NFA"""
    automaCap = NFA()
    automaCap.setSigma(automa.Sigma)
    #Creazione stati, Q = F = Q x Q -- I = (r, r , con r appartenente a Q)
    for statoAR in automaR.States:
        for statoRScc in automaSCC.States:
            nuovoStato = statoAR + ' - ' + statoRScc
            automaCap.addState(nuovoStato)
            automaCap.addFinal(automaCap.stateIndex(nuovoStato))
            if statoAR == statoRScc:
                automaCap.addInitial(automaCap.stateIndex(nuovoStato))
    for stato in statiAutoma: #Controllo analisi partendo da ogni singolo stato 
        automaCap = creaTransizioniAutomaACap(automaCap, stato, automa, automaR, automaSCC)
    automaCap.trim() #Rimuove stati non raggiungibili
    print '\nAutoma A^:\n{}\n'.format(automaCap)
    return automaCap